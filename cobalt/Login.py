import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import TimeoutException
from TestManager import TestManager
class AppLogin:

    def __init__(self, driver):
        self.driver = driver
        self.tst_admin_email = "CobaltABTestAdmin@ga-testing.com"
        self.tst_admin_password = "bKYsU33A@456"
        self.admin_email = "cobaltdltadmin02"
        self.admin_password = "Test123@"
        self.tst_attorney_email = "CobaltABTestAttorney@ga-testing.com"
        self.tst_attorney_password = "wjdJHXCD@123"
        self.attorney_email = "attorneydlt4"
        self.attorney_password = "Test123@"
        self.tst_assistant_email = "CobaltABTestAssistant@ga-testing.com"
        self.tst_assistant_password = "T883KqTu@123"
        self.tst_ccm_email = "CobaltABTestCCM@ga-testing.com"
        self.tst_ccm_password = "A27JxM3s@123"
        self.tst_eccm_email = "CobaltABTestECCM@ga-testing.com"
        self.tst_eccm_password = "uh8jLeNG@123"
        self.eccm_email = "eccmanager01"
        self.eccm_password = "Test123@"
        self.tst_km_email = "CobaltABTestKM@ga-testing.com"
        self.tst_km_password = "LMDfcDRx@123"
        self.tst_manager_email = "CobaltABTestManager@ga-testing.com"
        self.tst_manager_password = "vGrXg4vd@123"
        self.tst_hr_lead_email = "CobaltABTestHRLead@ga-testing.com"
        self.tst_hr_lead_password = "xJBWME5y"
        self.tst_hr_contact_email = "CobaltABTestHRContact@ga-testing.com"
        self.tst_hr_contact_password = "NeySbWVx@123"
        self.hr_contact_email = "hrcontact01"
        self.hr_contact_password = "Test123@"
        self.beneficiary_email = "beneficiary04"
        self.beneficiary_password = "Test123@"
        self.tst_beneficiary_email = "CobaltABTestBen@ga-testing.com"
        self.tst_beneficiary_password = "BrY7tD2V@123"

        base_url = TestManager().get_base_url()
        self.url = base_url
        self.is_sbx = self.check_env()

    def check_env(self):
        if ("tst" in self.url):
            self.is_sbx = False
        else:
            self.is_sbx = True

    def admin_login(self):
        if (self.is_sbx):
            self.login(self.admin_email, self.admin_password)
        else:
            self.tst_login(self.tst_admin_email, self.tst_admin_password)

    def beneficiary_login(self):
        if (self.is_sbx):
            self.login(self.beneficiary_email, self.beneficiary_password)
        else:
            self.tst_login(self.tst_beneficiary_email, self.tst_beneficiary_password)

    def attorney_login(self):
        if (self.is_sbx):
            self.login(self.attorney_email, self.attorney_password)
        else:
            self.tst_login(self.tst_attorney_email, self.tst_attorney_password)

    def assistant_login(self):
        if (self.is_sbx):
            self.login(self.assistant_email, self.assistant_password)
        else:
            self.tst_login(self.tst_assistant_email, self.tst_assistant_password)

    def ccm_login(self):
        if (self.is_sbx):
            self.login(self.ccm_email, self.ccm_password)
        else:
            self.tst_login(self.tst_ccm_email, self.tst_ccm_password)

    def eccm_login(self):
        if (self.is_sbx):
            self.login(self.eccm_email, self.eccm_password)
        else:
            self.tst_login(self.tst_eccm_email, self.tst_eccm_password)

    def km_login(self):
        if (self.is_sbx):
            self.login(self.km_email, self.km_password)
        else:
            self.tst_login(self.tst_km_email, self.tst_km_password)

    def manager_login(self):
        if (self.is_sbx):
            self.login(self.manager_email, self.manager_password)
        else:
            self.tst_login(self.tst_manager_email, self.tst_manager_password)

    def hr_lead_login(self):
        if (self.is_sbx):
            self.login(self.hr_lead_email, self.hr_lead_password)
        else:
            self.tst_login(self.tst_hr_lead_email, self.tst_hr_lead_password)

    def hr_contact_login(self):
        if (self.is_sbx):
            self.login(self.hr_contact_email, self.hr_contact_password)
        else:
            self.tst_login(self.tst_hr_contact_email, self.tst_hr_contact_password)
    

    def tst_login(self, email, password):
        """Login to tst env - must go through GA Portal"""
        browser = self.driver
        browser.get(self.url)
        try:
            #Send email
            element = WebDriverWait(browser, 20).until(
            EC.element_to_be_clickable((By.ID, "mat-input-0")))
            time.sleep(2)
            element.send_keys(email)
            time.sleep(4)

            #Send password
            element = WebDriverWait(browser, 8).until(
            EC.element_to_be_clickable((By.ID, "mat-input-1")))

            element.send_keys(password)
            time.sleep(3)

            #Click Login
            element = WebDriverWait(browser, 8).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, ".green")))

            element.click()
            time.sleep(8)

            #Skip secret questions/answers
            # if browser.find_elements(By.XPATH, "//button[contains(.,'Skip for Now')]"):
            #     print("element found")

            if browser.find_elements(By.XPATH, "//button[contains(.,'Skip for Now')]"):
                element = WebDriverWait(browser, 8).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Skip for Now')]")))
                element.click()
                time.sleep(5)
            
            if browser.find_elements(By.XPATH, "//button[contains(.,'Confirm')]"):
                element = WebDriverWait(browser, 8).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Confirm')]")))
                element.click()
                time.sleep(5)

            #Click on Immigration Tile
            element = WebDriverWait(browser, 8).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, ".links-no-child > .mat-list-item-content")))

            element.click()
            time.sleep(3)

            browser.switch_to.window(browser.window_handles[1])
            time.sleep(3)

            #Click Cobalt Test
            element = WebDriverWait(browser, 8).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, ".ng-star-inserted:nth-child(2) > .mat-list-item > .mat-list-item-content")))

            element.click()
            time.sleep(20)
    
            return browser

        except ElementClickInterceptedException:
                print("Element is obscured by another element in Login")
                return False

        except NoSuchElementException:
                print("Element could not be found")
                return False

        except TimeoutException:
                print("System timed out")
                return False


    def login(self, email, password):
        browser = self.driver
        browser.get(self.url)
        try:
            #Send username
            element = WebDriverWait(browser, 20).until(
            EC.element_to_be_clickable((By.NAME, "username")))
            element.send_keys(email)
            time.sleep(4)

            #Send password
            element = WebDriverWait(browser, 20).until(
            EC.element_to_be_clickable((By.NAME, "password")))
            element.send_keys(password)
            time.sleep(4)

            #Click login
            element = WebDriverWait(browser, 20).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, ".btn")))
            element.click()
            time.sleep(4)

            return browser


        except ElementClickInterceptedException:
                print("Element is obscured by another element")
                return False

        except NoSuchElementException:
                print("Element could not be found")
                return False

        except TimeoutException:
                print("System timed out")
                return False