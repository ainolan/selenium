import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import TimeoutException
import Login
from TestManager import TestManager

#Test Case ID: 175258

class BeneficiaryCasesPageTest(unittest.TestCase):

    def setUp(self):
        fireFoxOptions = webdriver.FirefoxOptions()
        fireFoxOptions.set_headless()
        self.driver  = webdriver.Firefox(firefox_options=fireFoxOptions)
        #self.driver = webdriver.Firefox()

        base_url = TestManager().get_base_url()

        self.url = base_url + "/#/dashboard/home"
        self.log_in = Login.AppLogin(self.driver)

    def test_viewing_cases(self):
        login_browser = self.log_in.beneficiary_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)

        time.sleep(5)

        assert "home" in browser.current_url

        #Verify user can see active cases
        browser.find_element_by_xpath("//active-case-list/div/div")
        #Verify bene can see case cards on home page
        #browser.find_element_by_link_text("New ALB Case")

        #Verify user can see when cases where initiated
        browser.find_element_by_xpath("//span[contains(.,'| Initiated on')]")
        
        #Click a case
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.ng-scope:nth-child(1) > .ng-isolate-scope > .case-card h1 > .ng-binding')))

        element.click()

        time.sleep(2)

        assert "casedetail" in browser.current_url

        #GO home
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Homepage')))

        element.click()

        time.sleep(2)

        assert "home" in browser.current_url

    def test_page_elements(self):
        login_browser = self.log_in.beneficiary_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)

        time.sleep(5)

        assert "home" in browser.current_url

        #Go to a case
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.ng-scope:nth-child(1) > .row > .col-md-8')))

        element.click()

        time.sleep(2)

        #Go to profile
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, "div:nth-child(2) > a")))

        element.click()

        time.sleep(2)

        #Go to cases page
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Cases')))

        element.click()

        time.sleep(2)

        #Go to cases page
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Cases')))

        element.click()

        time.sleep(2)

        assert "cases" in browser.current_url

        browser.find_element_by_xpath("//i[contains(.,'Destination Country/Location')]")
        browser.find_element_by_xpath("//i[contains(.,'Case Details')]")
        browser.find_element_by_xpath("//i[contains(.,'Opened Date')]")
        browser.find_element_by_xpath("//i[contains(.,'Filed Date')]")
        browser.find_element_by_xpath("//i[contains(.,'Decision')]")
        browser.find_element_by_xpath("//i[contains(.,'Closed Date')]")
        browser.find_element_by_xpath("//i[contains(.,'Attorney')]")
        browser.find_element_by_xpath("//i[contains(.,'Assistant')]")

        browser.find_element_by_xpath("//active-case-list/div/div")

        #Click a case
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.flex-item:nth-child(1) .ng-scope > .ng-scope')))

        element.click()

        time.sleep(2)

        assert "casedetail" in browser.current_url

        #Go back to user
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, 'div:nth-child(2) > a')))

        element.click()

        time.sleep(2)

        #Go back to cases
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Cases')))

        element.click()

        time.sleep(3)

        assert "cases" in browser.current_url



if __name__ == '__main__':
    unittest.main()