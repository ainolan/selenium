
# Read Me

## Description

UI and Regression tests for Cobalt. These test check to ensure that elements are where they should be on the 
application and that the application works as it should.

## How To Run


### Running Test Suite


To run the test suite, navigate to the cobalt directory (current directory) and run the following command:

    python TestSuite.py

### Running Individual Tests

To run an individual test class, then use the same above command but change the name of the file to the one you wish to run. Eg.

    python HCapTest.py

